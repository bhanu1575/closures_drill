

function cacheFunction(callbackFunction) {

    let cacheObject = {};
    
    return function (a,b) {

        if( Object.keys(cacheObject).includes(`${a},${b}`) ) {

            console.log('Found in cached Object...');
            return cacheObject[`${a},${b}`];

        } else{

            let result = callbackFunction(a,b);
            cacheObject[`${a},${b}`] = result;
            return result;

        }
    }
}



module.exports = cacheFunction;

