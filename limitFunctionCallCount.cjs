function limitFunctionCallCount(callbackFunction, n) {

    return function () {

        if(n != 0 ) {

            callbackFunction();
            n--;

        } else {

            throw new Error('Function call count limit reached');
        
        }
    }
}



module.exports = limitFunctionCallCount;




