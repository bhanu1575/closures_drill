const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');


function sayHello() {
    console.log('Hello, World!');
}

try {
    const letsSayHello = limitFunctionCallCount(sayHello, 3);

    letsSayHello();
    letsSayHello();
    letsSayHello();
    letsSayHello();
    letsSayHello();


} catch (error) {
    console.log(error.message)
}


