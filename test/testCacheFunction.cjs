const cacheFunction = require('../cacheFunction.cjs');


function add2Number (a,b) {
    console.log(`adding ${a} and ${b}.....`);
    return a+b;
}


let callTheFunctionToAdd2Number = cacheFunction(add2Number);

console.log(callTheFunctionToAdd2Number(1,2));
console.log(callTheFunctionToAdd2Number(10,20));

console.log(callTheFunctionToAdd2Number(1,2));
console.log(callTheFunctionToAdd2Number(10,20));



