function counterFactory() {

    let number = 1;
    let Object = {
        increment: function () {
            ++number;
            return number;
        },
        decrement: function () {
            --number;
            return number
        }
    }

    return Object;
}

module.exports = counterFactory;